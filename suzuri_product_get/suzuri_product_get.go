/*
suzuri_product_get 標準入力から SUZURI API materials のレスポンスを取得、そこから商品タイトルと URL を取得して標準出力に出力

input
	SuzuriResponse struct 参照.
output
  商品タイトル + 改行 + 商品ページ URL + 改行 + #SUZURI #creativecoding
*/
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type SuzuriResponse struct {
	Material struct {
		Id        int  `json:"id"`
		Published bool `json:"published"`
	} `json:"material"`
	Products []struct {
		Id        int    `json:"id"`
		Published bool   `json:"published"`
		Title     string `json:"title"`
		SampleUrl string `json:"sampleUrl"`
	} `json:"products"`
}

func main() {

	var stdout string

	// パイプ経由で標準入力を受け取る
	buf, err := ioutil.ReadAll(os.Stdin)
	errorLog(err)
	s := strings.TrimSpace(string(buf))
	if s == "" {
		errorLog(errors.New("no suzuri product data from stdin!"))
	}
	// json decode
	var resp SuzuriResponse
	err = json.Unmarshal(buf, &resp)
	if err != nil {
		errorLog(err)
	} else {
		if resp.Material.Published {
			for _, r := range resp.Products {
				if r.Published {
					stdout = r.Title + "\n" + r.SampleUrl + "\n"
					stdout += "#SUZURI #creativecoding"
				}
			}
		}

	}

	fmt.Println(string(stdout))

}

func errorLog(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
