package lib

import (
	"log"
)

func errorLog(e error) {
	if e != nil{
		log.Fatal(e)
	}
}
