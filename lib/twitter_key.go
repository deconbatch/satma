package lib

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type twitterKeys struct {
	ConsumerKey       string `json:"consumerKey"`
	ConsumerSecret    string `json:"consumerSecret"`
	AccessTokenKey    string `json:"accessTokenKey"`
	AccessTokenSecret string `json:"accessTokenSecret"`
	UserId            string `json:"userId"`
}

func ReadTwitterKey(keyPath string) (twitterKeys, error) {

	f, err := os.Open(keyPath)
	errorLog(err)
	defer f.Close()

	buf, err := ioutil.ReadAll(f)
	errorLog(err)

	var k twitterKeys
	err = json.Unmarshal(buf, &k)
	if err != nil {
		errorLog(err)
		return k, err
	}

	return k, nil

}
