package lib

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type suzuriKeys struct {
	Authorization string `json:"authorization"`
}

func ReadSuzuriKey(keyPath string) (suzuriKeys, error) {

	f, err := os.Open(keyPath)
	errorLog(err)
	defer f.Close()

	buf, err := ioutil.ReadAll(f)
	errorLog(err)

	var k suzuriKeys
	err = json.Unmarshal(buf, &k)
	if err != nil {
		errorLog(err)
		return k, err
	}

	return k, nil

}
