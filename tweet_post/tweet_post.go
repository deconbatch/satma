/*
tweet_post 標準入力から受けた内容をツイート

input
	ツイート内容 : 改行可
output
	ツイート結果
*/
package main

import (
	"errors"
	"fmt"
	"github.com/ChimeraCoder/anaconda"
	"io/ioutil"
	"log"
	"os"
	"satma/lib"
	"strings"
)

func main() {

	// パイプ経由で標準入力を受け取る
	b, err := ioutil.ReadAll(os.Stdin)
	errorLog(err)
	stdin := strings.TrimSpace(string(b))
	if stdin == "" {
		errorLog(errors.New("no post date from stdin!"))
	}

	// Twitter API キー取得
	tk, err := lib.ReadTwitterKey("./keys/twitterKey.json")  // path?
	errorLog(err)
	anaconda.SetConsumerKey(tk.ConsumerKey)
	anaconda.SetConsumerSecret(tk.ConsumerSecret)
	api := anaconda.NewTwitterApi(tk.AccessTokenKey, tk.AccessTokenSecret)

	// tweet post
	res, err := api.PostTweet(stdin, nil)
	errorLog(err)

	fmt.Println(res)

}

func errorLog(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
