/*
suzuri_tshirt_make 標準入力からタイトルと画像URL を元に SUZURI で Tシャツを作成。
レスポンスを標準出力に出力

input
	stdin struct 参照.
output
  SUZURI API のレスポンス
*/
package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"satma/lib"
	"strings"
)

type stdin struct {
	Title    string `json:"titile`
	ImageUrl string `json:imageUrl"`
}

type Product struct {
	ItemId     int    `json:"itemId"`
	Published  bool   `json:"published"`
	ResizeMode string `json:"resizeMode"`
}

type Material struct {
	Texture  string    `json:"texture"`
	Title    string    `json:"title"`
	Price    int       `json"price"`
	Products []Product `json:"products"`
}

func main() {

	var p Product
	var m Material

	// パイプ経由で標準入力を受け取る
	buf, err := ioutil.ReadAll(os.Stdin)
	errorLog(err)
	s := strings.TrimSpace(string(buf))
	if s == "" {
		errorLog(errors.New("no data from stdin to make T-shirt!"))
	}
	// json decode
	var input stdin
	err = json.Unmarshal(buf, &input)
	if err != nil {
		errorLog(err)
	} else {

		p = Product{
			ItemId:     1,
			Published:  true,
			ResizeMode: "contain",
		}
		m = Material{
			Texture:  input.ImageUrl,
			Title:    input.Title,
			Price:    300,
			Products: []Product{p},
		}
	}

	jsonBytes, err := json.Marshal(m)
	errorLog(err)

	// SUZURI API キー取得
	apiKey, err := lib.ReadSuzuriKey("./keys/suzuriKey.json")  // path?
	errorLog(err)

	// SUZURI post
	postUrl := "https://suzuri.jp/api/v1/materials"
	req, err := http.NewRequest("POST", postUrl, bytes.NewReader([]byte(jsonBytes)))
	errorLog(err)
	req.Header.Add("Authorization", "Bearer "+apiKey.Authorization)
	req.Header.Add("Content-Type", "application/json")
	client := new(http.Client)
	resp, err := client.Do(req)
	errorLog(err)
	defer resp.Body.Close()

	// レスポンスを標準出力に出力
	byteArray, err := ioutil.ReadAll(resp.Body)
	errorLog(err)
	fmt.Println(string(byteArray))

}

func errorLog(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
