/*
tweet_pick ユーザのタイムラインを取得し、標準入力から受けた語句がハッシュタグと合致するツイートを抽出。
本文の最初の行をタイトル、最初のメディアの URL をイメージ URL として JSON 形式で標準出力に出力

input
	検索対象のハッシュタグの語句 : #を除外した、空白を含まない語句
output
	stdout struct 参照.
*/
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/ChimeraCoder/anaconda"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"satma/lib"
	"strings"
)

type stdout struct {
	Title    string `json:"titile`
	ImageUrl string `json:imageUrl"`
}

func main() {

	// パイプ経由で標準入力を受け取る
	b, err := ioutil.ReadAll(os.Stdin)
	errorLog(err)
	stdin := strings.TrimSpace(string(b))
	if stdin == "" {
		errorLog(errors.New("no hashtag from stdin for match!"))
	}

	// Twitter API キー取得
	tk, err := lib.ReadTwitterKey("./keys/twitterKey.json") // path?
	errorLog(err)
	anaconda.SetConsumerKey(tk.ConsumerKey)
	anaconda.SetConsumerSecret(tk.ConsumerSecret)
	api := anaconda.NewTwitterApi(tk.AccessTokenKey, tk.AccessTokenSecret)

	// 最新 10 tweets、リプライ除外、リツイート除外
	vals := url.Values{}
	vals.Set("user_id", tk.UserId)
	vals.Add("count", "10")
	vals.Add("exclude_replies", "true")
	vals.Add("include_rts", "false")
	res, err := api.GetUserTimeline(vals)
	errorLog(err)

	var (
		title    string = ""
		mediaUrl string = ""
	)
	for _, tweet := range res {

		// ハッシュタグがマッチ？
		var match bool = existHashtag(tweet.Entities.Hashtags, stdin)

		// メディア URL 取得
		if match {
			for _, media := range tweet.Entities.Media {
				if media.Media_url != "" {
					mediaUrl = media.Media_url
					break
				}
			}
		}

		// 本文の最初の行を取得
		if mediaUrl != "" {
			title = strings.Split(tweet.FullText, "\n")[0]
			break
		}
	}

	if title != "" && mediaUrl != "" {
		s := stdout{
			Title:    title,
			ImageUrl: mediaUrl,
		}

		jsonBytes, err := json.Marshal(s)
		errorLog(err)

		fmt.Println(string(jsonBytes))
	}

}

// 引数こんな渡し方しないとダメ？
func existHashtag(hashtags []struct{Indices []int "json:\"indices\""; Text string "json:\"text\""}, key string) bool {
		for _, hashtag := range hashtags {
			if strings.Contains(hashtag.Text, key) {
				return true
				break
			}
		}
	return false
}	

func errorLog(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
